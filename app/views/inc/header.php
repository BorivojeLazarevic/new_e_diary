<h1>HEADER</h1>
<?php

if (isset($_SESSION['user_data']))
{
    $menu = new eeMenu;
    $menu->getMenu();

    echo '<ul>';

    echo '<li> <a href="/logout"> Odjavi se </a> </li>';

    for ($i=0; $i<count($menu->menu_title); $i++)
    {
        echo '<li> <a href="' . $menu->menu_url[$i] . '"> ' . $menu->menu_title[$i] . ' </a> </li>';
    }

    echo '</ul>';
}
else
{
    echo '
    <ul>
        <li> <a href="/login"> Uloguj se </a> </li>
    </ul>
    ';
}

?>